//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

static PUBLIC_IPS: async_mutex::Mutex<Option<PublicIPs>> = async_mutex::Mutex::new(None);

#[derive(Clone, Copy, Debug)]
pub struct PublicIPs {
    pub public_ip4_opt: Option<std::net::Ipv4Addr>,
    pub public_ip6_opt: Option<std::net::Ipv6Addr>,
}

#[allow(clippy::borrow_interior_mutable_const)]
pub async fn get_public_ips() -> PublicIPs {
    get_public_ips_inner(PUBLIC_IPS.lock().await).await
}

async fn get_public_ips_inner(
    mut guard: async_mutex::MutexGuard<'_, Option<PublicIPs>>,
) -> PublicIPs {
    use std::ops::Deref as _;
    if let Some(public_ips) = guard.deref() {
        *public_ips
    } else {
        // Get public IPs
        let (public_ip4_opt, public_ip6_opt) =
            futures_util::future::join(public_ip::addr_v4(), public_ip::addr_v6()).await;
        let public_ips = PublicIPs {
            public_ip4_opt,
            public_ip6_opt,
        };

        guard.replace(public_ips);

        public_ips
    }
}
