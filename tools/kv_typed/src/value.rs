//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! KV Typed Value trait

use crate::*;

/// Trait to be implemented by the collection value
#[cfg(not(feature = "explorer"))]
pub trait Value: 'static + AsBytes + Debug + FromBytes + PartialEq + Send + Sync + Sized {}

#[cfg(feature = "explorer")]
pub trait Value:
    'static + AsBytes + Debug + ExplorableValue + FromBytes + PartialEq + Send + Sync + Sized
{
}

#[cfg(not(feature = "explorer"))]
impl<T> Value for T where T: 'static + AsBytes + Debug + FromBytes + PartialEq + Send + Sync + Sized {}

#[cfg(feature = "explorer")]
impl<T> Value for T where
    T: 'static + AsBytes + Debug + ExplorableValue + FromBytes + PartialEq + Send + Sync + Sized
{
}

pub trait ValueZc: Value {
    type Ref: Sized + zerocopy::AsBytes + zerocopy::FromBytes;
}

impl ValueZc for () {
    type Ref = ();
}

macro_rules! impl_value_zc_for_numbers {
    ($($T:ty),*) => {$(
        impl ValueZc for $T {
            type Ref = Self;
        }
    )*};
}
impl_value_zc_for_numbers!(
    usize, u8, u16, u32, u64, u128, isize, i8, i16, i32, i64, i128, f32, f64
);

pub trait ValueSliceZc: Value {
    type Elem: Sized + zerocopy::AsBytes + zerocopy::FromBytes;

    fn prefix_len() -> usize {
        8
    }
}

impl ValueSliceZc for () {
    type Elem = ();

    fn prefix_len() -> usize {
        0
    }
}

impl ValueSliceZc for String {
    type Elem = u8;

    fn prefix_len() -> usize {
        0
    }
}

impl<T, E> ValueSliceZc for Vec<T>
where
    T: 'static
        + Copy
        + Debug
        + Default
        + Display
        + FromStr<Err = E>
        + PartialEq
        + Send
        + Sized
        + Sync
        + zerocopy::AsBytes
        + zerocopy::FromBytes,
    E: Error + Send + Sync + 'static,
{
    type Elem = T;

    fn prefix_len() -> usize {
        0
    }
}

macro_rules! impl_value_slice_zc_for_smallvec {
    ($($N:literal),*) => {$(
        impl<T, E> ValueSliceZc for SmallVec<[T; $N]>
        where
            T: 'static
                + Copy
                + Debug
                + Default
                + Display
                + FromStr<Err = E>
                + PartialEq
                + Send
                + Sized
                + Sync
                + zerocopy::AsBytes
                + zerocopy::FromBytes,
            E: Error + Send + Sync + 'static,
        {
            type Elem = T;

            fn prefix_len() -> usize {
                0
            }
        }
    )*};
}
impl_value_slice_zc_for_smallvec!(2, 4, 8, 16, 32, 64);

impl<T, E> ValueSliceZc for BTreeSet<T>
where
    T: 'static
        + Copy
        + Debug
        + Default
        + Display
        + FromStr<Err = E>
        + Ord
        + PartialEq
        + Send
        + Sized
        + Sync
        + zerocopy::AsBytes
        + zerocopy::FromBytes,
    E: Error + Send + Sync + 'static,
{
    type Elem = T;

    fn prefix_len() -> usize {
        0
    }
}
