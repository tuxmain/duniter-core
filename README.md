# Duniter Core

[![pipeline status](https://git.duniter.org/duniter/duniter-core/badges/master/pipeline.svg)](https://git.duniter.org/libs/dubp-rs-libs/-/commits/master)
[![dependency status](https://deps.rs/repo/github/duniter/duniter-core/status.svg)](https://deps.rs/repo/github/duniter/duniter-core)

Set of Rust libraries that implement the core Duniter logic and expose everything needed by Duniter modules.

This repository includes (non-hexaustive list):

- The configuration of a Duniter node (conf)
- The definition of the Duniter databases
- The block indexing code (dbs-write-ops)
- Distance rule checking (dubp-wot)
- Business processing for mempool inserts (mempool)
- The interface that modules must implement (module)

## Duniter modules

The main Duniter modules are in the **[nodes/rust/modules]** subgroup of **[duniter gitlab]**. Some modules may be located elsewhere, if their creator or maintainer has decided so.

To implement your own module, you just need to define a Rust type that implements the **[DuniterModule]** trait.

## Other repositories

Duniter's code is separated into several git repositories:

* **[dubp-rs-libs]** contains the logic common to Duniter and its customers.
* **[duniter-core]** contains the core code of Duniter.
* The gitlab subgroup **[nodes/rust/modules]** contains the main Duniter modules code (gva, admin, etc).
* The **[duniter]** repository contains the "official" implementations of the "duniter-cli" and "duniter-desktop" programs with their default modules (also contains the historical implementation being migrated).

[DuniterModule]: https://git.duniter.org/nodes/rust/duniter-core/blob/main/module/src/lib.rs#L41
[duniter gitlab]: https://git.duniter.org
[dubp-rs-libs]: https://git.duniter.org/libs/dubp-rs-libs
[duniter-core]: https://git.duniter.org/nodes/rust/duniter-core
[duniter]: https://git.duniter.org/nodes/typescript/duniter
[nodes/rust/modules]: https://git.duniter.org/nodes/rust/modules
